#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  9 17:28:08 2018

@author: earanda

Script to convert FreeFem++ mesh and solution files in pdf graphic file 
built with pgfplots

Use: python ff2pgf.py mesh [--onlymesh] [--solb solbfile] [--p1] [-b n1 n2 ... ] -B [c1 c2 ...]
                           [-i n1 n2 ... ] -I [c1 c2 ...]
    where n1, n2 are label (boundary or inner) and c1 c2 are valid colors in latex
    
"""

codigobase = """
\\RequirePackage{{luatex85}}
\\documentclass[]{{standalone}}
\\usepackage{{pgfplots}}
\\usetikzlibrary{{pgfplots.colormaps}}
\\usepgfplotslibrary{{patchplots}}
\\pgfplotsset{{compat=1.15,
    colormap/blackwhite}}
\\begin{{document}}
\\begin{{tikzpicture}}
\\begin{{axis}}{options}
{plots}
\\end{{axis}}
\\end{{tikzpicture}}
\\end{{document}}
"""    
opcion0 = """[colormap={reverse winter}{indices of colormap={
 \\pgfplotscolormaplastindexof{blackwhite},...,0 of blackwhite}
    },
 view={0}{90},axis equal,enlargelimits=false,axis x line=none,
 axis y line=none]
"""
opcions = "[colormap/bone,view={60}{45}]"

opcion1 = "[hide axis=true,line width=0.1pt,axis equal,enlargelimits=false]"

plot1 = "\\addplot3[patch,shader=interp] file {grafo.txt};"
plot2 = "\\addplot[patch,faceted color=blue,colormap={mymap}{rgb=(1,1,1); rgb=(1,1,1)}] file {malla.txt};"
plot3 = "\\addplot[line width=1.2pt,{color}] file {{{bdfile}}};"
plot4 = "\\addplot[patch,faceted color={color},colormap={{mymap}}{{rgb=(1,1,1); rgb=(1,1,1)}}] file {{{infile}}};"

import subprocess
import argparse

def checkerror(p):
    if p:
        print("An error has ocurred")
        exit()

def procesor(cmd,opt,pl):
    p = subprocess.Popen([cmd],shell=True,close_fds=True)
    p.wait()
    checkerror(p.poll())
    with open("file.tex","w") as f:
        f.write(codigobase.format(options=opt,plots=pl))    
    


parser = argparse.ArgumentParser()

parser.add_argument('mesh', action="store")
parser.add_argument('--solb', action="store", dest="filesolb", default=None,
                    help='Create solution graph')
parser.add_argument('--onlymesh', action="store_true",default=False,
                    help='Create only mesh without remarking labels')
parser.add_argument('--p1', action="store_true",default=False,
                    help='Case of solution .solb in P1')
parser.add_argument('-b', nargs='+', dest='bdlabels',default=[],
                    help='Boundary labels on mesh')
parser.add_argument('-B', nargs='+', dest='bdcolors',default=[],
                    help='Colors of boundary labels')
parser.add_argument('-i', nargs='+', dest='inlabels',default=[],
                    help='Inner region labels on mesh')
parser.add_argument('-I', nargs='+', dest='incolors',default=[],
                    help='Colors of inner regions')
parser.add_argument('--output',action="store",dest="output",default="graph.pdf",
                    help='Name of PDF file')
parser.add_argument('--keep',action="store_true",default=False,
                    help='Do not remove all auxiliary files')


args = parser.parse_args()

# Check if list of labels are same lenght as colors
if (len(args.bdcolors) != len(args.bdlabels)) or (len(args.inlabels) != len(args.incolors)):
    print("Inconsistent data: different number of labels and colors")
    exit()

if args.filesolb:
    cmd = "FreeFem++ -ne ff2txt.edp -mesh " + args.mesh + " -solb " + args.filesolb
    opt = opcion0
    if args.p1:
        cmd += ' -p1 1'
        opt = opcions        
    procesor(cmd,opt,plot1)
elif args.onlymesh:
    cmd = "FreeFem++ -ne ff2txt.edp -mesh " + args.mesh 
    procesor(cmd,opcion1,plot2)
else:
    plots = ''
    for x,y in zip(args.bdlabels,args.bdcolors):
        cmd = "FreeFem++ -ne ff2txt.edp -mesh " + args.mesh + " -bdlabel " + x + " -no 1"
        p = subprocess.Popen([cmd],shell=True,close_fds=True)
        p.wait()
        checkerror(p.poll())
        name = 'bdmalla'+x+'.txt'
        plots += plot3.format(color=y,bdfile=name) + '\n'
    for x,y in zip(args.inlabels,args.incolors):
        cmd = "FreeFem++ -ne ff2txt.edp -mesh " + args.mesh + " -innerlabel " + x + " -no 1"
        p = subprocess.Popen([cmd],shell=True,close_fds=True)
        p.wait()
        checkerror(p.poll())
        name = 'inmalla'+x+'.txt'
        plots += plot4.format(color=y,infile=name) + '\n'
    cmd = "FreeFem++ -ne ff2txt.edp -mesh " + args.mesh 
    plots = plot2 + '\n' + plots + '\n'
    procesor(cmd,opcion1,plots)
    
# Latex compilation
p = subprocess.Popen(["lualatex file.tex"],shell=True,close_fds=True)
p.wait()
checkerror(p.poll())
p = subprocess.Popen(["mv file.pdf " + args.output],shell=True,close_fds=True)
p.wait()
checkerror(p.poll())
#
# cleaning

if not args.keep:
    p = subprocess.Popen(["rm file.tex file.log file.aux"],shell=True,close_fds=True)
    p.wait()
    if args.filesolb:
        p = subprocess.Popen(["rm grafo.txt malla.txt"],shell=True,close_fds=True)
        p.wait()
    else:
        p = subprocess.Popen(["rm malla.txt"],shell=True,close_fds=True)
        p.wait()
        for x in args.bdlabels:
             name = 'bdmalla'+x+'.txt'
             p = subprocess.Popen(["rm " + name],shell=True,close_fds=True)
             p.wait()
        for x in args.inlabels:
             name = 'inmalla'+x+'.txt'
             p = subprocess.Popen(["rm " + name],shell=True,close_fds=True)
             p.wait()
            
   
