
# ff2pgf
Python script to convert mesh and solution files of FreeFem++ to PDF using pgfplots

It needs FreeFem++ and lualatex installed. This scripts read a .msh mesh FreeFem++ file and optionally a .solb solution file
or boundary/inner labels and creates a PDF file with pgfplots


# How to use
For mesh:
~~~
$ python  ffpgf.py meshfile [-b n1 n2 ...] -B [colorb1 colorb2 ...] [-i m1 m2 ...] -I [colori1 colori2 ...] --output out.pdf [--keep]
~~~
For functions:
~~~
$ python ffpgf.py meshfile --solb solution [--p1] --output out.pdf [--keep]
~~~

# Examples

- Create the necessary files. In the examples folder there is a FreeFem++ code to generate a mesh and P0 and P1 solutions. 

~~~ 
$ FreeFem++ examples/freefem_example.edp
~~~

Then in the examples folder we have the files `lbracket.msh`, `sol-lbracketP0.solb` and `sol-lbracketP1.solb`

- Now the script can generate a pdf with the mesh:

~~~
$ python ff2pgf.py examples/lbracket.msh --output mesh.pdf
~~~

If you want to highlight a part of the boundary you can pass the corresponding labels and colors
~~~
$ python ff2pgf.py examples/lbracket.msh -b 1 2 -B red green --output meshcolor.pdf
~~~

Also, labelled region can be highlighted
~~~
$ python ff2pgf.py examples/lbracket.msh -b 3 -B red -i 10 15 -I magenta green --output fullcolor.pdf
~~~

- Plot a P0 contour plot function

~~~
$ python ff2pgf.py examples/lbracket.msh --solb examples/sol-lbracketP0.solb --output p0.pdf
~~~

- Plot a P1 function

~~~
$ python ff2pgf.py examples/lbracket.msh --solb examples/sol-lbracketP1.solb --p1 --output p1.pdf
~~~

# Additional note

- The script removes all intermediate files created but you can keep them using `--keep` option

